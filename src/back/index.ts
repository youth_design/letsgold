import express from 'express';
import productsRouter from './routes/api/products';
import ordersRouter from './routes/api/orders';
import authRouter from './routes/api/auth';
import { checkEnv } from './utils/serverUtils';

const morgan = require('morgan');
const bodyParser = require('body-parser');
require('dotenv').config();

require('./db');

checkEnv(process.env);
const app = express();
const port = process.env.BACKEND_PORT;

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'));

app.use(bodyParser.json());

app.use('/api/products', productsRouter);
app.use('/api/orders', ordersRouter);
app.use('/api/auth', authRouter);

app.listen(port, () => console.log(`Running on port ${port}`));
