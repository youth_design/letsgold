import express from 'express';
import loginHandler from '../../handlers/loginHandler';
import logoutHandler from '../../handlers/logoutHandler';
import refreshTokenHandler from '../../handlers/refreshTokenHandler';
import authByTokenHandler from '../../handlers/authByTokenHandler';
import authenticateToken from '../../middlewares/authenticateToken';

const authRouter = express.Router();

authRouter.post('/login', loginHandler);

authRouter.post('/logout', logoutHandler);

authRouter.post('/token', refreshTokenHandler);

authRouter.post('/byToken', authenticateToken, authByTokenHandler);

export default authRouter;
