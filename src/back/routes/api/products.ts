import express from 'express';
import { getProductByIdHandler, getProductsHandler } from '../../handlers/productsHandler';
import { removeProductHandler } from '../../handlers/removeProductHandler';
import authenticateToken from '../../middlewares/authenticateToken';

const productsRouter = express.Router();

productsRouter.get('/', getProductsHandler);

productsRouter.get('/:id', getProductByIdHandler);
productsRouter.post('/delete', authenticateToken, removeProductHandler);

export default productsRouter;
