import express from 'express';
import { createOrderHandler, getOrderById } from '../../handlers/ordersHandler';

const ordersRouter = express.Router();

ordersRouter.get('/:orderId', getOrderById);

ordersRouter.post('/', createOrderHandler);

export default ordersRouter;
