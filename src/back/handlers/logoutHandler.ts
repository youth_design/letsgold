import { Response, Request } from 'express';
import jwt from 'jsonwebtoken';
import { errorResponse, successResponse } from '../utils/serverUtils';
import {
  CREDENTIALS_ERROR, SUCCESS_BODY, TECH_ERROR,
} from '../utils/messages';
import { getUser } from '../models/userModel';

const logoutHandler = async (req: Request, res: Response) => {
  const REFRESH_TOKEN: string = req.body?.REFRESH_TOKEN?.toString();
  if (!REFRESH_TOKEN) {
    return res.json(errorResponse(CREDENTIALS_ERROR, 403));
  }
  const { REFRESH_TOKEN_SECRET: RTS } = process.env;
  try {
    return jwt.verify(REFRESH_TOKEN, RTS, async (err, user) => {
      if (err?.name === 'TokenExpiredError') {
        return res.json(successResponse(SUCCESS_BODY));
      }
      if (err || !user?.NAME) {
        console.log(err);
        return res.json(errorResponse(CREDENTIALS_ERROR, 403));
      }
      const userFromDB = await getUser({ NAME: user.NAME });
      if (userFromDB?.REFRESH_TOKENS?.length) {
        const refreshTokensNew = userFromDB.REFRESH_TOKENS.filter((i) => i !== REFRESH_TOKEN);
        await userFromDB.update({ $set: { REFRESH_TOKENS: refreshTokensNew } });
        return res.json(successResponse(SUCCESS_BODY));
      }
      return res.json(errorResponse(CREDENTIALS_ERROR, 403));
    });
  } catch (e) {
    console.log(e);
    return res.json(errorResponse(TECH_ERROR));
  }
};

export default logoutHandler;
