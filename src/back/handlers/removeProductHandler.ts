import { Request, Response } from 'express';
import { errorResponse, successResponse } from '../utils/serverUtils';
import { INVALID_INPUT, SUCCESS_BODY, TECH_ERROR } from '../utils/messages';
import { removeProduct } from '../models/productsModel';

export const removeProductHandler = async (req: Request, res: Response) => {
  let { id } = req.body;
  id = `${id}`;
  if (!id) {
    return res.json(errorResponse(INVALID_INPUT));
  }
  try {
    const result = await removeProduct(id);
    if (result) {
      return res.send(successResponse(SUCCESS_BODY));
    }
    return res.json(errorResponse(TECH_ERROR));
  } catch {
    return res.json(errorResponse(TECH_ERROR));
  }
};
