import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import { errorResponse, successResponse } from '../utils/serverUtils';
import { CREDENTIALS_ERROR, TECH_ERROR } from '../utils/messages';
import { UserModel } from '../models/userModel';

const authByTokenHandler = (req: Request, res: Response) => {
  const authHeader = req.headers.authorization;
  const accessToken = authHeader && authHeader.split(' ')[1];
  if (!accessToken) {
    return res.json(errorResponse(CREDENTIALS_ERROR, 403));
  }
  const { ACCESS_TOKEN_SECRET: ATS } = process.env;
  try {
    return jwt.verify(accessToken, ATS, async (err, user) => {
      if (err || !user?.NAME) {
        return res.json(errorResponse(CREDENTIALS_ERROR, 403));
      }
      const userFromDB = await UserModel.findOne({ NAME: user.NAME });
      if (userFromDB) {
        const { NAME, IS_ADMIN } = userFromDB;
        return res.json(
          successResponse({ NAME, IS_ADMIN }),
        );
      }
      return res.json(errorResponse(CREDENTIALS_ERROR, 403));
    });
  } catch (e) {
    return res.json(errorResponse(TECH_ERROR, 403));
  }
};

export default authByTokenHandler;
