import { Request, Response } from 'express';
import {
  errorResponse,
  comparePasswordWithHash,
  genereateAccessToken,
  genereateRefreshToken, successResponse,
} from '../utils/serverUtils';
import { CREDENTIALS_ERROR, INVALID_INPUT, TECH_ERROR } from '../utils/messages';
import { getUser } from '../models/userModel';

const loginHandler = async (req: Request, res: Response) => {
  const { NAME, PASSWORD } = req.body;
  if (!NAME || !PASSWORD) return res.json(errorResponse(INVALID_INPUT));
  try {
    const user = { NAME };

    const userFromDB = await getUser(user);
    if (!userFromDB) return res.json(errorResponse(CREDENTIALS_ERROR, 403));

    const hash = userFromDB.PASSWORD;
    if (hash) {
      const isMatch = await comparePasswordWithHash(PASSWORD, hash);
      if (!isMatch) return res.json(errorResponse(CREDENTIALS_ERROR, 403));
    } else {
      return res.json(errorResponse(CREDENTIALS_ERROR, 403));
    }
    const IS_ADMIN = userFromDB.IS_ADMIN || false;
    const ACCESS_TOKEN = genereateAccessToken(user, process.env.ACCESS_TOKEN_SECRET);
    const REFRESH_TOKEN = genereateRefreshToken(user, process.env.REFRESH_TOKEN_SECRET);
    const refreshTokens = userFromDB.REFRESH_TOKENS || [];
    refreshTokens.push(REFRESH_TOKEN);
    await userFromDB.update({ $set: { REFRESH_TOKENS: refreshTokens } });

    return res.json(successResponse({ ACCESS_TOKEN, REFRESH_TOKEN, IS_ADMIN }));
  } catch {
    return res.json(errorResponse(TECH_ERROR));
  }
};

export default loginHandler;
