import { Request, Response } from 'express';
import { errorResponse, successResponse } from '../utils/serverUtils';
import { Order, Product } from '../../utils/entities';
import { saveOrder, getOrderById as _getOrderById } from '../models/ordersModel';
import { getProductById } from '../models/productsModel';
import { MISS_REQUIRED_FIELD_DECORATOR, TECH_ERROR } from '../utils/messages';

export const getOrderById = async (req: Request, res: Response) => {
  try {
    const orderId = req.params.orderId || '';
    const order: Order | false = await _getOrderById(orderId);
    res.json(successResponse(order));
    return res.end();
  } catch (e) {
    console.log(e);
    res.json(errorResponse(TECH_ERROR));
    return res.end();
  }
};

export const createOrderHandler = async (req : Request, res: Response) => {
  try {
    const order : Order = req.body;
    const { FORM_VALUES } = order;
    const product: Product | false = await getProductById(order.PRODUCT_ID);
    if (product) {
      Object.entries(FORM_VALUES).forEach(([name]) => {
        const fieldFromDB = product.ORDER_FORM_FIELDS.find((i) => i.name === name);
        if (!fieldFromDB) {
          delete FORM_VALUES[name];
        }
      });
      // eslint-disable-next-line no-restricted-syntax
      for (const field of product.ORDER_FORM_FIELDS) {
        if (field.required && !FORM_VALUES[field.name]) {
          res.json(errorResponse(MISS_REQUIRED_FIELD_DECORATOR(field.name)));
          res.end();
          return;
        }
      }
      const r = await saveOrder(order);
      res.json(successResponse({ result: r }));
      res.end();
      return;
    }
    res.json(errorResponse(TECH_ERROR));
    res.end();
    return;
  } catch (e) {
    console.log('Error', e);
    res.json(errorResponse(TECH_ERROR));
    res.end();
  }
};
