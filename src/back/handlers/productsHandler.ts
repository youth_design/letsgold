import { Request, Response } from 'express';
import { getAllProducts, getProductById } from '../models/productsModel';
import { successResponse } from '../utils/serverUtils';
import { Product } from '../../utils/entities';

export const getProductsHandler = async (req: Request, res: Response) => {
  const products: Product[] = await getAllProducts();

  res.json(successResponse(products));
  return res.end();
};

export const getProductByIdHandler = async (req: Request, res: Response) => {
  const { id } = req.params;
  const product : Product | false = await getProductById(id);

  res.json(successResponse(product));
  return res.end();
};
