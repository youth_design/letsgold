import { Request, Response } from 'express';
import jwt from 'jsonwebtoken';
import {
  cutTokensList,
  errorResponse,
  genereateAccessToken,
  genereateRefreshToken, getUserData,
  successResponse,
} from '../utils/serverUtils';
import {
  CREDENTIALS_ERROR, INVALID_TOKEN, TECH_ERROR,
} from '../utils/messages';
import { getUser } from '../models/userModel';
import { UserData } from '../../utils/entities';

const refreshTokenHandler = (req: Request, res: Response) => {
  const { REFRESH_TOKEN } = req.body;
  const rfToken = String(REFRESH_TOKEN);
  if (!REFRESH_TOKEN) {
    return res.json(errorResponse(CREDENTIALS_ERROR, 403));
  }
  const {
    REFRESH_TOKEN_SECRET: RTS,
    ACCESS_TOKEN_SECRET: ATS,
  } = process.env;

  try {
    return jwt.verify(rfToken, RTS, async (err, user) => {
      if (err?.name === 'TokenExpiredError') {
        return res.json(errorResponse(INVALID_TOKEN, 401));
      }
      if (err || typeof user?.NAME !== 'string') {
        return res.json(errorResponse(CREDENTIALS_ERROR, 403));
      }
      const userFromDB = await getUser({ NAME: user.NAME });

      if (userFromDB?.REFRESH_TOKENS?.length) {
        const refreshTokenMatch = userFromDB.REFRESH_TOKENS.find((i) => i === REFRESH_TOKEN);
        if (!refreshTokenMatch) return res.json(errorResponse(CREDENTIALS_ERROR, 403));
        const refreshTokensFiltered = userFromDB.REFRESH_TOKENS.filter((i) => i !== REFRESH_TOKEN);
        const userData = getUserData(user);
        const refreshTokenNew = genereateRefreshToken(userData as UserData, RTS);
        const refreshTokensNew = cutTokensList([...refreshTokensFiltered, refreshTokenNew]);
        await userFromDB.update({ $set: { REFRESH_TOKENS: refreshTokensNew } });
        const ACCESS_TOKEN = genereateAccessToken(userData as UserData, ATS);
        return res.json(successResponse({ ACCESS_TOKEN, REFRESH_TOKEN: refreshTokenNew }));
      }
      return res.json(errorResponse(CREDENTIALS_ERROR, 403));
    });
  } catch (e) {
    console.log(e);
    return res.json(errorResponse(TECH_ERROR));
  }
};

export default refreshTokenHandler;
