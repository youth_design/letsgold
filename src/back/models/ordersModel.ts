import mongoose from 'mongoose';
import { Order } from '../../utils/entities';

const { Schema, model } = mongoose;

const orderSchema = new Schema<Order>({
  PRODUCT_ID: { type: String, required: true },
  COUNT: { type: Number, required: true, min: 1 },
  FORM_VALUES: { type: Object, required: true },
});

const OrderModel = model<Order>('Order', orderSchema);

export const saveOrder = async (order: Order) : Promise<string | false> => {
  try {
    const _order = new OrderModel(order);
    const r = await _order.save();
    return r?._id || false;
  } catch (e) {
    console.log('Error', e);
    return false;
  }
};

export const getOrderById = async (orderId: string) : Promise<Order | false> => {
  try {
    const order = await OrderModel.findById(new mongoose.Types.ObjectId(orderId));
    if (order) {
      return order;
    }
    return false;
  } catch (e) {
    console.log('Error', e);
    return false;
  }
};
