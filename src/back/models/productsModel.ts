import mongoose from 'mongoose';
import { Product } from '../../utils/entities';

const { Schema, model } = mongoose;

const productSchema = new Schema<Product>({
  NAME: { type: String, required: true },
  DESCRIPTION: { type: String, required: false },
  COST_PER_PIECE: { type: Number, required: true },
  IMAGE_PATH: { type: String, required: false },
  INCREMENT_MULTIPLIER: { type: Number, required: true },
  ORDER_FORM_FIELDS: { type: Array, required: true },
  ACTIVE: { type: Boolean, required: true },
});

const ProductModel = model<Product>('Product', productSchema);

export const getAllProducts = async () : Promise<Product[]> => {
  try {
    return await ProductModel.find();
  } catch (e) {
    console.log('Error', e);
    return [];
  }
};

export const saveProduct = async (product: Product) : Promise<boolean> => {
  try {
    const pr = new ProductModel(product);
    await pr.save();
    return true;
  } catch (e) {
    console.log('Error', e);
    return false;
  }
};

export const getProductById = async (id: string) : Promise<Product | false> => {
  try {
    const product = await ProductModel.findById(new mongoose.Types.ObjectId(id));
    if (product) {
      return product;
    }
  } catch (e) {
    console.log('Error', e);
  }
  return false;
};

export const removeProduct = async (id: string) : Promise<true | false> => {
  try {
    const result = await ProductModel.findByIdAndRemove(new mongoose.Types.ObjectId(id));
    if (result) {
      return true;
    }
  } catch (e) {
    console.log('Error', e);
    return false;
  }
  return false;
};
