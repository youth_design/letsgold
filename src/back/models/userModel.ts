import mongoose, { Document } from 'mongoose';
import { User } from '../../utils/entities';

const { Schema, model } = mongoose;

const userSchema = new Schema<User>({
  NAME: { type: String, required: true },
  PASSWORD: { type: String, required: true },
  IS_ADMIN: { type: Boolean, required: false },
  REFRESH_TOKENS: { type: [], required: false },
});

export const UserModel = model<User>('User', userSchema);

type UserQuery = {
  NAME: string
}

type UserDoc = User & Document;
export const getUser = async (user: UserQuery) : Promise<UserDoc | null> => {
  try {
    return UserModel.findOne(user);
  } catch (e) {
    throw Error(e);
  }
};

export const saveUser = async (user: User) : Promise<boolean> => {
  try {
    const _user = new UserModel(user);
    await _user.save();
    return true;
  } catch (e) {
    console.log(e);
    return false;
  }
};
