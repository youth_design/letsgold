import { EnvType } from '../utils/serverUtils';

declare global {

  // eslint-disable-next-line no-unused-vars
  namespace NodeJS {
    // eslint-disable-next-line no-unused-vars
    interface ProcessEnv extends EnvType {}
  }
}

export { };
