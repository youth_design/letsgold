import mongoose from 'mongoose';

mongoose.connect(`${process.env.MONGO_URL}/${process.env.DB_NAME}`, { useNewUrlParser: true, useUnifiedTopology: true }, (err) => {
  if (err) {
    console.log('Error', err);
  } else {
    console.log('MongoDB connected!');
  }
});
