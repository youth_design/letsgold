import { _createProducts } from './products';
import { _createUser } from './users';

require('dotenv').config();

require('../../db');

(async () => {
  await _createProducts(13);
  await _createUser();
})();
