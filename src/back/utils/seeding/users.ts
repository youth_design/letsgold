import { saveUser } from '../../models/userModel';
import { createHash } from '../serverUtils';

export const _createUser = async () => {
  const user = {
    NAME: 'admin',
    PASSWORD: await createHash('12345678', 10),
    IS_ADMIN: true,
  };
  await saveUser(user);
};
