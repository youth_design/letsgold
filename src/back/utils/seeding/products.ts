import { Product } from '../../../utils/entities';
import { saveProduct } from '../../models/productsModel';

export const _createProducts = async (productsCount: number) => {
  for (let i = 1; i <= productsCount; i += 1) {
    const pr: Product = {
      NAME: `ЗОЛОТО Wowcircle 3.3.5a x${i}`,
      COST_PER_PIECE: i * 5,
      IMAGE_PATH: '/wow.jpg',
      DESCRIPTION: 'Цена за 1000 золотых',
      INCREMENT_MULTIPLIER: i * 2,
      ACTIVE: Boolean(i % 3),
      ORDER_FORM_FIELDS: [
        {
          id: 'CHAR_NAME',
          name: 'CHAR_NAME',
          type: 'text',
          required: true,
          placeholder: 'Имя персонажа',
        },
        {
          id: 'EMAIL',
          name: 'EMAIL',
          type: 'email',
          required: true,
          placeholder: 'Ваш e-mail',
        },
        {
          id: 'FACTION',
          name: 'FACTION',
          type: 'select',
          required: true,
          placeholder: 'Сторона',
          options: [{ value: 'horde', label: 'Орда' }, { value: 'alliance', label: 'Альянс' }],
        },
        {
          id: 'CONTACTS',
          name: 'CONTACTS',
          type: 'text',
          required: false,
          placeholder: 'Контакты (необязательно)',
        },
      ],
    };
    saveProduct(pr);
  }
};
