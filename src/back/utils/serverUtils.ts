import bcrypt from 'bcrypt';
import jwt, { JwtPayload } from 'jsonwebtoken';
import { UserData } from '../../utils/entities';

export const successResponse = <E> (body: E) => ({
  body,
  status: 200,
});

export const errorResponse = <E> (message: E, code = 500) => ({
  error: {
    code,
    message,
  },
});

export const comparePasswordWithHash = async (password: string, hash:string): Promise<boolean> => (
  bcrypt.compare(password, hash)
);

export const createHash = async (value: string, salt:number): Promise<string> => (
  bcrypt.hash(value, salt)
);

export const genereateRefreshToken = (user: UserData, token: string) => (
  jwt.sign(user, token, { expiresIn: process.env.REFRESH_TOKEN_LIFE || '30d' })
);

export const genereateAccessToken = (user: UserData, token: string) => (
  jwt.sign(user, token, { expiresIn: process.env.ACCESS_TOKEN_LIFE || '15m' })
);

const INITIAL_ENV_REQUIRED = [
  'ACCESS_TOKEN_SECRET',
  'REFRESH_TOKEN_SECRET',
  'DB_NAME',
  'MONGO_URL',
  'NEXT_PUBLIC_BACKEND_PORT',
  'BACKEND_PORT',
];

export interface EnvType {
  ACCESS_TOKEN_SECRET: string,
  REFRESH_TOKEN_SECRET: string,
  DB_NAME: string,
  MONGO_URL: string,
  NEXT_PUBLIC_BACKEND_PORT: string,
  BACKEND_PORT: string,
  [key: string]: string | undefined
}

export const checkEnv = (env: EnvType) => {
  INITIAL_ENV_REQUIRED.forEach((key) => {
    if (!env[key]) {
      process.exit(1);
      throw new Error(`${key} not found in env!`);
    }
  });
};

export const cutTokensList = (list: string[], limit = 5) => list.slice(-limit);

export const getUserData = ({ NAME }:JwtPayload) => ({ NAME });
