export const TECH_ERROR = 'Возникла неизвестная техническая ошибка';
export const CREDENTIALS_ERROR = 'Неверный логин или пароль';
export const INVALID_INPUT = 'Переданы неверные параметры';
export const INVALID_TOKEN = 'Невалидный токен';
export const MISS_REQUIRED_FIELD_DECORATOR = (fieldName: string) => ` В параметрах отсутствует обязательное поле:  ${fieldName}`;
export const SUCCESS_BODY = { message: 'OK' };
