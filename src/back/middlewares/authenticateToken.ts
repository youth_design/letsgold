import { NextFunction, Response } from 'express';
import jwt from 'jsonwebtoken';
import { RequestAuthenticateToken, UserData } from '../../utils/entities';
import { errorResponse } from '../utils/serverUtils';
import { CREDENTIALS_ERROR, INVALID_TOKEN, TECH_ERROR } from '../utils/messages';

const authenticateToken = (req: RequestAuthenticateToken, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;
  const token = authHeader && authHeader.split(' ')[1];
  if (!token) {
    return res.json(errorResponse(CREDENTIALS_ERROR, 403));
  }
  try {
    return jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      if (err?.name === 'TokenExpiredError') {
        return res.json(errorResponse(INVALID_TOKEN, 401));
      }
      if (err || !user) {
        return res.json(errorResponse(CREDENTIALS_ERROR, 403));
      }
      req.user = user as UserData;
      return next();
    });
  } catch {
    return res.json(errorResponse(TECH_ERROR, 500));
  }
};
export default authenticateToken;
