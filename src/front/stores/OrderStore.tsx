import {
  makeAutoObservable,
} from 'mobx';

import { RootStore } from './RootStore';
import { Order } from '../../utils/entities';

import { getOrderById as _getOrderById } from '../api/methods';

export class OrderStore {
  root: RootStore;

  constructor(root: RootStore) {
    this.root = root;
    makeAutoObservable(this, {}, { autoBind: true });
  }

  order: Order | null = null;

  loading = false;

  error : null | string = null;

  * getOrderById(orderId: string) {
    try {
      this.loading = true;
      const order : Order | false = yield _getOrderById(orderId);
      if (order) {
        this.order = order;
      } else {
        this.error = 'Заказ не найден';
      }
    } catch (e) {
      console.log(e);
      this.error = 'Возникла неизвестная техническа ошибка';
    } finally {
      this.loading = false;
    }
  }
}
