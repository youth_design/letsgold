import { AuthStore } from './AuthStore';
import { ProductsStore } from './ProductsStore';
import { OrderStore } from './OrderStore';

export interface RootStoreHydration {
  [key: string]: unknown;
}

export class RootStore {
  authStore: AuthStore

  productsStore: ProductsStore

  orderStore: OrderStore

  constructor() {
    this.authStore = new AuthStore(this);
    this.productsStore = new ProductsStore(this);
    this.orderStore = new OrderStore(this);
  }

  hydrate(data: RootStoreHydration) {
    /* ToDo: add hydration here */
    console.log(this, data);
  }
}
