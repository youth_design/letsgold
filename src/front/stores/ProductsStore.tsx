import {
  makeObservable, observable, flow,
} from 'mobx';
import { RootStore } from './RootStore';
import { getAllProducts, getProductById, removeProduct } from '../api/methods';
import { Product } from '../../utils/entities';

type Products = Array<Product> | null;

export class ProductsStore {
  root: RootStore;

  constructor(root: RootStore) {
    this.root = root;
    makeObservable(this);
    this.getAllProducts = this.getAllProducts.bind(this);
    this.removeProduct = this.removeProduct.bind(this);
    this.getAllProductsFlow = this.getAllProductsFlow.bind(this);
    this.removeProductFlow = this.removeProductFlow.bind(this);
    this.getProductFlow = this.getProductFlow.bind(this);
    this.getProduct = this.getProduct.bind(this);
  }

  @observable products: Products = null;

  @observable product: Product = null;

  @observable error: null | string = null;

  @observable loading = false;

  * getAllProducts() {
    try {
      this.loading = true;
      const products = yield getAllProducts();
      if (products) {
        this.products = products;
      } else {
        this.error = 'Возникла неизвестная техническая ошибка';
      }
    } catch (e) {
      console.log(e);
      this.error = 'Возникла неизвестная техническая ошибка';
    } finally {
      this.loading = false;
    }
  }

  getAllProductsFlow = flow(this.getAllProducts);

  * getProduct(id: string) {
    try {
      this.loading = true;
      const product = yield getProductById(id);
      if (product) {
        this.product = product;
      } else {
        this.error = 'Возникла неизвестная техническая ошибка';
      }
    } catch (e) {
      console.log(e);
      this.error = 'Возникла неизвестная техническая ошибка';
    } finally {
      this.loading = false;
    }
  }

  getProductFlow = flow(this.getProduct);

  * removeProduct(id: string) {
    try {
      this.loading = true;
      const removed = yield removeProduct(id);
      if (removed) {
        this.products = this.products.filter((i) => i._id !== id);
      } else {
        this.error = 'Возникла неизвестная техническая ошибка';
      }
    } catch (e) {
      console.log(e);
      this.error = 'Возникла неизвестная техническая ошибка';
    } finally {
      this.loading = false;
    }
  }

  removeProductFlow = flow(this.removeProduct);
}
