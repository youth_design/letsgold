import {
  action, makeObservable, observable, flow,
} from 'mobx';
import { RootStore } from './RootStore';
import { logout } from '../api/methods';
import { clearTokens, getToken } from '../helpers/Utils';

type UserData = {
  NAME: string | null,
  IS_ADMIN: boolean
} | null;

export class AuthStore {
  root: RootStore;

  constructor(root: RootStore) {
    this.root = root;
    makeObservable(this);
    this.logoutGenerator = this.logoutGenerator.bind(this);
  }

  @observable user = null

  @observable isLogged = false

  @action setUser(userData: UserData) {
    this.user = userData;
    this.isLogged = true;
  }

  @action setIsLogged(isLogged: boolean) {
    this.isLogged = isLogged;
  }

  * logoutGenerator() {
    try {
      const REFRESH_TOKEN = getToken('REFRESH_TOKEN');
      const result = yield logout(REFRESH_TOKEN);
      if (result) {
        clearTokens();
        this.user = null;
      }
    } catch (e) {
      console.log(e);
    }
  }

  logoutFlow = flow(this.logoutGenerator)
}
