export const isServer = typeof window === 'undefined';

export const setTokens = (ACCESS_TOKEN: string, REFRESH_TOKEN: string) : true | null => {
  if (isServer) return null;
  window.localStorage.setItem('ACCESS_TOKEN', ACCESS_TOKEN);
  window.localStorage.setItem('REFRESH_TOKEN', REFRESH_TOKEN);
  return true;
};

type TokensType = {
  ACCESS_TOKEN: string,
  REFRESH_TOKEN: string,
}
export const getTokens = () : TokensType | null => {
  if (isServer) return null;
  const ACCESS_TOKEN = window.localStorage.getItem('ACCESS_TOKEN');
  const REFRESH_TOKEN = window.localStorage.getItem('REFRESH_TOKEN');
  return { ACCESS_TOKEN, REFRESH_TOKEN };
};

export const clearTokens = () : true | null => {
  if (isServer) return null;
  window.localStorage.removeItem('ACCESS_TOKEN');
  window.localStorage.removeItem('REFRESH_TOKEN');
  return true;
};

export const getToken = (token : string) : string | null => {
  if (isServer) return null;
  return window.localStorage.getItem(token);
};
