module.exports = {
  async rewrites() {
    return [
      {
        source: '/api/:path*',
        destination: `http://localhost:${process.env.BACKEND_PORT}/api/:path*`,
      },
    ];
  },
};
