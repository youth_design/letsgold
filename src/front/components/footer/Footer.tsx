import React from 'react';
import Link from 'next/link';
import s from './Footer.module.sass';

const Footer = () => (
  <footer>
    <div className={s.footerContent}>
      <div className={`${s.item} ${s.link}`}>
        <Link href="/agreement">УСЛОВИЯ СОГЛАШЕНИЯ</Link>
      </div>
      <div className={`${s.item} ${s.link}`}>
        <Link href="/privacy-policy">ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ</Link>
      </div>
      <a className={s.item} href="https://passport.webmoney.ru/asp/CertView.asp?wmid=141962520550" target="_blank" rel="noreferrer">
        <img src="https://www.webmoney.ru/img/icons/88x31_wm_blue.png" alt="www.megastock.com" />
      </a>
      <div className={s.item}>
        @ 2021 LETSGOLD.
      </div>
    </div>
  </footer>
);

export default Footer;
