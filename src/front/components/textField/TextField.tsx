import React from 'react';
import {
  Control,
  Controller,
  FieldErrors,
  UseControllerProps,
} from 'react-hook-form';
import { Input, InputProps } from 'antd';
import InputLabel from '../inputLabel/InputLabel';
import s from './TextField.module.sass';

type TextFieldProps<T> = UseControllerProps<T> & {
  control: Control<T>,
  label?: React.ReactNode | string,
  required?: boolean,
  errors?: FieldErrors,
  id: string,
} & InputProps;

const TextField = <T extends unknown>(props : TextFieldProps <T>) => {
  const {
    name, control, label, id, required, errors, ...rest
  } = props;

  const error = errors[name]?.message || false;

  return (
    <div className={s.container}>
      {label && (
        <InputLabel htmlFor={id} required={required}>{label}</InputLabel>
      )}
      <Controller <T>
        name={name}
        control={control}
        render={({ field }) => <Input className={`${error ? s.errorInput : ''}`} {...field} {...rest} id={id} />}
      />
      {error && <div className={s.error}>{error}</div>}
    </div>
  );
};

TextField.defaultProps = {
  label: null,
  required: false,
  errors: {},
};

export default TextField;
