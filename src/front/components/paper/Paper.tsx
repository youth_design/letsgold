import React from 'react';

import styles from './Paper.module.sass';

interface PaperProps {
    children: React.ReactNode,
}

const Paper = ({ children }: PaperProps) => (
  <div className={styles.paper}>
    {children}
  </div>
);

export default Paper;
