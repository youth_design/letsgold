import React from 'react';
import s from './AdminContentWrapper.module.sass';

type PropsType = {
  title: string,
  children: React.ReactNode,
}

const AdminContentWrapper = (props: PropsType) => {
  const { title, children } = props;

  return (
    <div className={s.wrapper}>
      <div>{title}</div>
      {children}
    </div>
  );
};

export default AdminContentWrapper;
