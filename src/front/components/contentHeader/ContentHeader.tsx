import React from 'react';
import s from './ContentHeader.module.sass';

interface ContentHeaderProps {
    children: React.ReactNode,

}

const ContentHeader = (props : ContentHeaderProps) => {
  const {
    children,
  } = props;

  return (
    <div className={s.container}>
      <div className={s.imageWrapper} />
      <div className={s.imageOverlay} />
      <div className={s.bgContent}>
        {children}
      </div>
    </div>
  );
};

export default ContentHeader;
