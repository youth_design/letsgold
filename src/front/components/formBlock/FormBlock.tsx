import React from 'react';

interface FormBlockProps {
  customMarginTop?: number,
  children: React.ReactNode,
  className?: string,
}

const FormBlock = (props: FormBlockProps) => {
  const {
    customMarginTop,
    children,
    className,
  } = props;
  return (
    <div className={`${className}`} style={{ marginTop: `${customMarginTop}px` }}>
      {children}
    </div>
  );
};

FormBlock.defaultProps = {
  customMarginTop: 5,
  className: 24,
};

export default FormBlock;
