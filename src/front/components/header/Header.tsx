import React from 'react';
import { observer } from 'mobx-react-lite';
import s from './Header.module.sass';
import NavLink from '../navLink/NavLink';
import { useAuthStore } from '../../providers/rootProvider';

const Header = () => {
  const authStore = useAuthStore();
  const isAuthorized = authStore.user;
  const handleLogout = () => authStore.logoutFlow();

  return (
    <header className={s.header}>
      <div>LETSGOLD</div>
      <ul className={s.navigation}>
        <li>
          <NavLink className={s.link} activeClassName={s.linkActive} href="/">Главная</NavLink>
        </li>
        <li>
          <NavLink className={s.link} activeClassName={s.linkActive} href="/products">Товары</NavLink>
        </li>
        <li>
          <NavLink className={s.link} activeClassName={s.linkActive} href="#">О нас</NavLink>
        </li>
        <li><NavLink className={s.link} activeClassName={s.linkActive} href="#">Гарантии</NavLink></li>
        <li><NavLink className={s.link} activeClassName={s.linkActive} href="#">FAQ</NavLink></li>
        <li><NavLink className={s.link} activeClassName={s.linkActive} href="#">Контакты</NavLink></li>
        <li><NavLink className={s.link} activeClassName={s.linkActive} href="#">Отзывы</NavLink></li>
        {isAuthorized
        && (
          <li>
            <button type="button" onClick={handleLogout} className={s.buttonLink}>Выйти</button>
          </li>
        )}
      </ul>
    </header>
  );
};

export default observer(Header);
