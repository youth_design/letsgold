import React from 'react';
import s from './ContentContainer.module.sass';

interface ContentContainerProps {
  children: React.ReactNode,
}

const ContentContainer = ({ children }: ContentContainerProps) => (
  <div className={s.wrapper}>
    <div className={s.containerPage}>
      {children}
    </div>
  </div>
);

export default ContentContainer;
