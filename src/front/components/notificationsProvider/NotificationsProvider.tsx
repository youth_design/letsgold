import React, {
  createContext, useContext, useEffect, useState,
} from 'react';
import { Alert } from 'antd';
import { generateGuid } from '../../../utils/utils';
import { passCreateNotification } from '../../api/api';

import s from './NotificationProvider.module.sass';
import FormBlock from '../formBlock/FormBlock';

interface NotificationsProviderProps {
  children: React.ReactNode
}

interface NotificationContext {
  // eslint-disable-next-line no-unused-vars
  createNotification?: CreateNotification,
}

interface NotificationProps {
  timeout?: number,
  variant: 'success' | 'warning' | 'error' | 'info',
  content: string | React.ReactNode,
}

export interface CreateNotification {
  // eslint-disable-next-line no-unused-vars
  (notification: NotificationProps): void,
}

interface NotificationsState {
  [id: string]: NotificationProps
}

export const notificationsContext = createContext<NotificationContext>({});

export const useNotification = () => useContext(notificationsContext);

const NotificationsProvider = (props: NotificationsProviderProps) => {
  const { children } = props;
  const [notifications, setNotifications] = useState<NotificationsState>({});

  const closeNotification = (guid: string) => {
    setNotifications((prevState) => {
      const newState = { ...prevState };
      delete newState[guid];
      return newState;
    });
  };

  const createNotification = (notification : NotificationProps) => {
    const guid = generateGuid();

    setNotifications((prev) => ({
      ...prev,
      [guid]: notification,
    }));

    // Не вяжемся на асинхронное измененеие состояния notification
    setTimeout(() => {
      closeNotification(guid);
    }, notification.timeout);
  };

  useEffect(() => {
    passCreateNotification(createNotification);
  }, []);
  return (
    <notificationsContext.Provider value={{ createNotification }}>
      <div className={s.wrapper}>
        {Object.entries(notifications)
          .map(([guid, notification]) => (
            <FormBlock customMarginTop={10}>
              <Alert
                key={guid}
                type={notification.variant}
                message={notification.content}
                closable
                onClose={() => closeNotification(guid)}
              />
            </FormBlock>
          ))}
      </div>
      {children}
    </notificationsContext.Provider>
  );
};

export default NotificationsProvider;
