import React from 'react';
import { Select, SelectProps } from 'antd';
import {
  Control, Controller, UseControllerProps,
} from 'react-hook-form';

import s from './SelectField.module.sass';
import InputLabel from '../inputLabel/InputLabel';

type SelectFieldProps<T> = SelectProps<unknown> & UseControllerProps<T> &{
  name: string,
  control: Control<T>,
  label?: React.ReactNode | string,
  required?: boolean,
  children: React.ReactNode[] | React.ReactNode,
}

const SelectField = <T extends unknown>(props: SelectFieldProps<T>) => {
  const {
    name, control, label, required, id, children, ...rest
  } = props;
  return (
    <div>
      {label && (
        <InputLabel htmlFor={id} required={required}>
          {label}
        </InputLabel>
      )}
      <Controller
        name={name}
        control={control}
        rules={{ required }}
        render={({ field }) => (
          <Select {...field} {...rest} id={id} className={s.select}>
            {children}
          </Select>
        )}
      />
    </div>
  );
};

SelectField.defaultProps = {
  label: null,
  required: false,
};

export default SelectField;
