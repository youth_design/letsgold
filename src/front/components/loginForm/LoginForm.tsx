import React, { useState } from 'react';
import { Button } from 'antd';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { observer } from 'mobx-react-lite';
import FormBlock from '../formBlock/FormBlock';
import s from './LoginForm.module.sass';
import { login } from '../../api/methods';
import TextField from '../textField/TextField';
import useYupValidationResolver from '../../helpers/useYupValidationResolver';
import { AuthStore } from '../../stores/AuthStore';
import { setTokens } from '../../helpers/Utils';

const schema = yup.object().shape({
  NAME: yup.string()
    .required('Имя обязательно')
    .max(50, 'Превышено максимальное колличество символов')
    .min(3),
  PASSWORD: yup.string()
    .required('Пароль обязателен')
    .max(50, 'Превышено максимальное колличество символов')
    .min(4),
});
// type FormData = yup.InferType<typeof schema>;

interface LoginFormTypes {
  authStore: AuthStore,
}
type LoginFormValues = {
  NAME: string,
  PASSWORD: string,
}

const LoginForm = (props : LoginFormTypes) => {
  const { authStore } = props;

  const resolver = useYupValidationResolver(schema);

  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginFormValues>({ resolver });

  const [isFetching, setFetching] = useState(false);

  const onSubmit = async (FORM_VALUES : LoginFormValues) => {
    const { NAME, PASSWORD } = FORM_VALUES;
    try {
      setFetching(true);
      const result = await login({ NAME, PASSWORD });
      if (result) {
        const { ACCESS_TOKEN, REFRESH_TOKEN, IS_ADMIN } = result;
        setTokens(ACCESS_TOKEN, REFRESH_TOKEN);
        authStore.setUser({ NAME, IS_ADMIN });
      }
    } catch (e) {
      console.log(e);
    } finally {
      setFetching(false);
    }
  };

  return (
    <section className={s.wrapper}>
      <form onSubmit={handleSubmit(onSubmit)}>
        <FormBlock>
          <TextField
            control={control}
            name="NAME"
            id="NAME"
            type="TEXT"
            label="Логин"
            errors={errors}
            required
          />
        </FormBlock>
        <FormBlock customMarginTop={5}>
          <TextField
            control={control}
            name="PASSWORD"
            id="PASSWORD"
            type="PASSWORD"
            label="Пароль"
            errors={errors}
            required
          />
        </FormBlock>
        <FormBlock>
          <Button disabled={isFetching} htmlType="submit">Вход</Button>
        </FormBlock>
      </form>
    </section>
  );
};
export default observer(LoginForm);
