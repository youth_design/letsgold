import Link from 'next/link';
import { useRouter } from 'next/router';
import React from 'react';

interface NavLinksProps {
  activeClassName?: string,
  className?: string,
  href: string,
  children: React.ReactNode,
}

const NavLink = ({
  href, activeClassName = '', className = '', children,
} : NavLinksProps) => {
  const router = useRouter();

  return (
    <Link href={href}>
      {/* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
      <a className={`${className}  ${router.pathname === href ? activeClassName : ''}`}>
        {children}
      </a>
    </Link>
  );
};

NavLink.defaultProps = {
  activeClassName: '',
  className: '',
};

export default NavLink;
