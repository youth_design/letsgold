import React from 'react';

import s from './InputLabel.module.sass';

interface InputLabelProps {
  htmlFor?: string | undefined,
  children: React.ReactNode,
  required?: boolean,
}

const InputLabel = (props: InputLabelProps) => {
  const { children, htmlFor, required } = props;
  return (
    <div>
      <label htmlFor={htmlFor}>
        {children}
        {required ? <span className={s.requiredSymbol}> *</span> : null}
      </label>
    </div>
  );
};

InputLabel.defaultProps = {
  required: false,
  htmlFor: undefined,
};

export default InputLabel;
