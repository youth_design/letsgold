import React from 'react';
import {
  Control,
  Controller,
  FieldErrors,
  UseControllerProps,
} from 'react-hook-form';
import { Checkbox, CheckboxProps } from 'antd';
import InputLabel from '../inputLabel/InputLabel';
import s from './Checkbox.module.sass';

type Props<T> = UseControllerProps<T> & {
  control: Control<T>,
  label?: React.ReactNode | string,
  required?: boolean,
  errors?: FieldErrors,
  id: string,
} & CheckboxProps;

const CheckBox = <T extends unknown>(props : Props <T>) => {
  const {
    name, control, label, id, required, errors,
  } = props;

  const error = errors[name]?.message || false;

  return (
    <div className={s.container}>
      {label && (
        <InputLabel htmlFor={id} required={required}>{label}</InputLabel>
      )}
      <Controller <T>
        name={name}
        control={control}
        render={({ field: { value, ...field } }) => (
          <Checkbox
            checked={!!value}
            className={`${s.checkbox} ${error ? s.errorInput : ''}`}
            {...field}
            id={id}
          />
        )}
      />
      {error && <div className={s.error}>{error}</div>}
    </div>
  );
};

CheckBox.defaultProps = {
  label: null,
  required: false,
  errors: {},
};

export default CheckBox;
