import React from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';
import { Button } from 'antd';
import { Product } from '../../../utils/entities';
import useYupValidationResolver from '../../helpers/useYupValidationResolver';
import s from './ProductForm.module.sass';
import FormBlock from '../formBlock/FormBlock';
import TextField from '../textField/TextField';
import Checkbox from '../checkBox/Checkbox';

type ProductFields = Omit<Product, '_id' | 'ORDER_FORM_FIELDS'>

type Props = {
  product: ProductFields,
  onSave: () => void
}

const schema = yup.object().shape({

});

const ProductForm = (props: Props) => {
  const { product, onSave } = props;

  const resolver = useYupValidationResolver(schema);
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<ProductFields>({ resolver, defaultValues: product });

  const onSubmit = async (FORM_VALUES : ProductFields) => {
    console.log(FORM_VALUES);
    console.log(onSave);
  };

  return (
    <div>
      <section className={s.wrapper}>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormBlock>
            <TextField
              control={control}
              name="NAME"
              id="NAME"
              type="TEXT"
              label="Название товара"
              errors={errors}
              required
            />
          </FormBlock>
          {product.DESCRIPTION
          && (
          <FormBlock>
            <TextField
              control={control}
              name="DESCRIPTION"
              id="DESCRIPTION"
              label="Описание"
              errors={errors}
              required
            />
          </FormBlock>
          )}
          <FormBlock>
            <TextField
              control={control}
              name="COST_PER_PIECE"
              id="COST_PER_PIECE"
              type="COST_PER_PIECE"
              label="Цена за единицу"
              errors={errors}
              required
            />
          </FormBlock>
          <FormBlock>
            <TextField
              control={control}
              name="INCREMENT_MULTIPLIER"
              id="INCREMENT_MULTIPLIER"
              type="INCREMENT_MULTIPLIER"
              label="Шаг"
              errors={errors}
              required
            />
          </FormBlock>
          <FormBlock>
            <Checkbox
              control={control}
              name="ACTIVE"
              id="ACTIVE"
              type="ACTIVE"
              label="Активен"
              errors={errors}
              required
            />
          </FormBlock>
          <FormBlock>
            <Button htmlType="submit">Сохранить</Button>
          </FormBlock>
        </form>
      </section>
    </div>
  );
};

export default ProductForm;
