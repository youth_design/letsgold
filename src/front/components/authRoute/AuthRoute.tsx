import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import Router from 'next/router';
import { Spin } from 'antd';
import { useAuthStore } from '../../providers/rootProvider';
import { authByToken } from '../../api/methods';
import s from './AuthRoute.module.sass';

const PROTECTED_ROUTES = ['/admin'];

const AuthRoute = ({ children }) => {
  const authStore = useAuthStore();

  useEffect(() => {
    const getAuth = async () => {
      const user = await authByToken();
      if (user) {
        authStore.setUser(user);
      } else if (PROTECTED_ROUTES.includes(window.location.pathname)) {
        await Router.push('/login');
        authStore.setIsLogged(true);
      } else {
        authStore.setIsLogged(true);
      }
    };
    getAuth();
  }, []);

  if (!authStore.isLogged) {
    return (
      <div className={s.wrapperLoader}>
        <Spin size="large" />
      </div>
    );
  }
  return children;
};

export default observer(AuthRoute);
