import React, {
  SyntheticEvent, useEffect, useRef, useState,
} from 'react';
import { NextRouter, useRouter } from 'next/router';
import { Product } from '../../../utils/entities';

import styles from './ProductCard.module.sass';

interface IProductCardProps {
    product: Product,
}

const ProductCard = (props: IProductCardProps) => {
  const {
    product,
  } = props;

  const router: NextRouter = useRouter();

  const inputRef: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);
  const inputRefPrice: React.RefObject<HTMLInputElement> = useRef<HTMLInputElement>(null);
  const [count, setCount] = useState<number>(product.INCREMENT_MULTIPLIER);
  const [countPrice, setCountPrice] = useState<number>(
    product.COST_PER_PIECE * product.INCREMENT_MULTIPLIER,
  );

  useEffect(() => {
    if (inputRef.current) {
      inputRef.current.value = count.toString();
    }
    setCountPrice(count * product.COST_PER_PIECE);
  }, [count]);

  useEffect(() => {
    if (inputRefPrice.current) {
      inputRefPrice.current.value = countPrice.toString();
    }
    setCount(Math.floor(countPrice / product.COST_PER_PIECE));
  }, [countPrice]);

  const handleInputBlur = (e: SyntheticEvent<HTMLInputElement>) => {
    const newCount = parseInt(e.currentTarget.value, 10);
    const ref = inputRef.current;
    if (ref) {
      if (newCount < product.INCREMENT_MULTIPLIER) {
        ref.value = count.toString();
      } else {
        setCount(parseInt(ref.value, 10));
      }
    }
  };

  const handleInputPriceBlur = (e: SyntheticEvent<HTMLInputElement>) => {
    const newCount = parseInt(e.currentTarget.value, 10);
    const ref = inputRefPrice.current;
    if (ref) {
      if (newCount < product.COST_PER_PIECE) {
        ref.value = countPrice.toString();
      } else {
        setCountPrice(parseInt(ref.value, 10));
      }
    }
  };

  const handleBuyButtonClick = async () => {
    await router.push(`/checkout/${product._id}?productCount=${count}`);
  };

  const incrementCount = () => setCount((prevCount) => prevCount + product.INCREMENT_MULTIPLIER);

  const decrementCount = () => setCount(
    (prevCount) => (prevCount > product.INCREMENT_MULTIPLIER
      ? prevCount - product.INCREMENT_MULTIPLIER
      : prevCount),
  );

  return (
    <div className={styles.wrapper}>
      <div className={styles.cardImage} style={{ backgroundImage: `url("${product.IMAGE_PATH}")` }} />
      <div className={styles.content}>
        <h4 className={styles.header}>{product.NAME}</h4>
        <div className={styles.description}>{product.DESCRIPTION}</div>
        <div className={styles.price}>
          {product.COST_PER_PIECE}
          ₽
        </div>
        <div className={styles.counter}>
          <button type="button" onClick={decrementCount}>-</button>
          <input
            type="number"
            ref={inputRef}
            onBlur={handleInputBlur}
            min={product.INCREMENT_MULTIPLIER}
            max={10000000000000000}
            name={product._id}
            id={product._id}
            step={product.INCREMENT_MULTIPLIER}
            inputMode="numeric"
          />
          <button type="button" onClick={incrementCount}>+</button>
        </div>
        <div className={styles.description}>
          Итого:
          <input
            type="number"
            ref={inputRefPrice}
            onBlur={handleInputPriceBlur}
            min={product.INCREMENT_MULTIPLIER}
            max={10000000000000000}
            name={product._id}
            id={product.NAME}
            step={product.INCREMENT_MULTIPLIER}
            inputMode="numeric"
            className={styles.inputSum}
          />
          {' '}
          ₽
        </div>
        <div className={styles.buyButtonWrapper}>
          <button
            type="button"
            onClick={handleBuyButtonClick}
            className={styles.buyButton}
          >
            Купить
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductCard;
