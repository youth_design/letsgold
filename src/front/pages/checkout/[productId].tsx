import React, { useState } from 'react';
import { GetStaticPaths, GetStaticProps, InferGetStaticPropsType } from 'next';
import { ParsedUrlQuery } from 'querystring';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { Button, Select, Table } from 'antd';
import { createOrder, getAllProducts, getProductById } from '../../api/methods';
import ContentHeader from '../../components/contentHeader/ContentHeader';
import Paper from '../../components/paper/Paper';

import styles from './Checkout.module.sass';
import {
  EmailFieldParams, FormValues, Product, SelectFieldParams, TextFieldParams,
} from '../../../utils/entities';
import ContentContainer from '../../components/conetentContainer/ContentContainer';
import TextField from '../../components/textField/TextField';
import SelectField from '../../components/selectField/SelectField';
import FormBlock from '../../components/formBlock/FormBlock';

const { Option } = Select;

interface CheckoutProps {
    product: Product | false,
    productId: string,
}

const Checkout = (props: InferGetStaticPropsType<typeof getStaticProps>) => {
  const [isFetching, setIsFetching] = useState<boolean>(false);

  const router = useRouter();
  const productCount: string = Array.isArray(router.query.productCount)
    ? router.query.productCount[0]
    : router.query.productCount || '1';
  const { product } = props;

  const { control, handleSubmit } = useForm<FormValues>();

  const onSubmit = async (FORM_VALUES : FormValues) => {
    setIsFetching(true);
    const response = await createOrder({
      COUNT: parseInt(productCount, 10),
      PRODUCT_ID: (product && product?._id) || '',
      FORM_VALUES,
    });
    setIsFetching(false);
    if (response) {
      await router.push(`/orders/${response}`);
    }
  };

  const getFormField = (field: TextFieldParams | EmailFieldParams | SelectFieldParams) => {
    switch (field.type) {
      case 'text':
      case 'email':
        return (
          <TextField
            control={control}
            name={field.name}
            id={field.id}
            type={field.type}
            label={field.placeholder}
            required={field.required}
          />
        );
      case 'select':
        return (
          <SelectField
            control={control}
            name={field.name}
            id={field.id}
            label={field.placeholder}
            required={field.required}
          >
            {field.options.map((option) => (
              <Option
                value={option.value}
                key={option.value}
              >
                {option.label}
              </Option>
            ))}
          </SelectField>
        );
      default:
        return null;
    }
  };

  if (!product) {
    return null;
  }

  const tableTemplate = [
    {
      title: 'Товар',
      dataIndex: 'product',
      key: 'products',
    },
    {
      title: 'Подытог',
      dataIndex: 'result',
      key: 'result',
    },
  ];

  const tableContent = [
    {
      key: '1',
      product: `${product.NAME} x${productCount}`,
      result: `${parseFloat(productCount) * product.COST_PER_PIECE} ₽`,
    },
    {
      key: '2',
      product: 'Итого',
      result: `${parseFloat(productCount) * product.COST_PER_PIECE} ₽`,
    },
  ];

  return (
    <div>
      <ContentHeader>
        <h1>ТОВАРЫ — LETSGOLD</h1>
      </ContentHeader>
      <ContentContainer>
        <div className={styles.wrapper}>
          <Paper>
            <h2>Детали оплаты</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
              {product.ORDER_FORM_FIELDS.map(
                (field) => <FormBlock key={field.name}>{getFormField(field)}</FormBlock>,
              )}
              <FormBlock>
                <Button
                  type="primary"
                  size="large"
                  htmlType="submit"
                  disabled={isFetching}
                >
                  Подтвердить заказ
                </Button>
              </FormBlock>
            </form>
          </Paper>
          <Paper>
            <Table
              dataSource={tableContent}
              columns={tableTemplate}
              pagination={false}
              bordered
            />
          </Paper>
        </div>
      </ContentContainer>
    </div>
  );
};

interface RouteParams extends ParsedUrlQuery{
    productId: string,
}

export const getStaticPaths: GetStaticPaths = async () => {
  const products = await getAllProducts();

  const paths = products.map((product) => ({ params: { productId: product._id } }));
  return { paths, fallback: false };
};

export const getStaticProps: GetStaticProps<CheckoutProps> = async (context) => {
  const { productId } = context.params as RouteParams;
  const product = await getProductById(productId);

  return {
    props: {
      productId,
      product,
    },
    revalidate: 60, // need to check in production
  };
};

export default Checkout;
