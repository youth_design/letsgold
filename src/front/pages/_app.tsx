import '../shared/styles.sass';

import { AppProps } from 'next/app';

import React from 'react';
import 'antd/dist/antd.css';
import type { NextPage } from 'next';
import type { ReactElement, ReactNode } from 'react';
import NotificationsProvider from '../components/notificationsProvider/NotificationsProvider';
import { RootStoreProvider } from '../providers/rootProvider';
import AuthRoute from '../components/authRoute/AuthRoute';
import Header from '../components/header/Header';
import Footer from '../components/footer/Footer';

type NextPageWithLayout = NextPage & {
  getLayout?: (page: ReactElement) => ReactNode
}

type AppPropsWithLayout = AppProps & {
  Component: NextPageWithLayout
}

const _App = ({ Component, pageProps } : AppPropsWithLayout) => (
  <RootStoreProvider>
    <NotificationsProvider>
      <AuthRoute>
        <Header />
        {
          Component.getLayout
            ? Component.getLayout(<Component {...pageProps} />)
            : <Component {...pageProps} />
        }
        <Footer />
      </AuthRoute>
    </NotificationsProvider>
  </RootStoreProvider>

);

export default _App;
