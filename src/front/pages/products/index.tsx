import React from 'react';
import { InferGetStaticPropsType, GetStaticProps } from 'next';
import { Product } from '../../../utils/entities';
import ProductCard from '../../components/productCard/ProductCard';
import ContentHeader from '../../components/contentHeader/ContentHeader';

import styles from './Products.module.sass';
import ContentContainer from '../../components/conetentContainer/ContentContainer';
import { getAllProducts } from '../../api/methods';

interface ProductsProps {
    products: Product[],
}

const Products = ({ products }: InferGetStaticPropsType<typeof getStaticProps>) => (
  <div>
    <ContentHeader>
      <h1>ТОВАРЫ — LETSGOLD</h1>
    </ContentHeader>
    <ContentContainer>
      <div className={styles.content}>
        {products.map((product) => <ProductCard key={product._id} product={product} />)}
      </div>
    </ContentContainer>
  </div>
);

export const getStaticProps: GetStaticProps<ProductsProps> = async () => {
  const products: Product[] = await getAllProducts();
  return {
    props: {
      products,
    },
    revalidate: 60,
  };
};

export default Products;
