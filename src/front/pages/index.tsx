import React from 'react';
import ContentHeader from '../components/contentHeader/ContentHeader';

const IndexPage = () => (
  <div>
    <ContentHeader>
      <div>
        <h1>ДОБРО ПОЖАЛОВАТЬ В ИНТЕРНЕТ-МАГАЗИН LETSGOLD</h1>
      </div>
    </ContentHeader>
  </div>
);

export default IndexPage;
