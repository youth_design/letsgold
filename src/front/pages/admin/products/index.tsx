import React, { useEffect } from 'react';
import { Button, Table } from 'antd';
import { observer } from 'mobx-react-lite';
import Router from 'next/router';
import { useProductsStore } from '../../../providers/rootProvider';
import getLayoutAdmin from '../../../layouts/getLayoutAdmin';
import AdminContentWrapper from '../../../components/adminContentWrapper/AdminContentWrapper';

const Products = () => {
  const productsStore = useProductsStore();
  const {
    loading, products, getAllProductsFlow, removeProductFlow,
  } = productsStore;

  useEffect(() => {
    getAllProductsFlow();
  }, []);

  const columns = [
    {
      title: 'Название',
      key: 'NAME',
      dataIndex: 'NAME',
    },
    {
      title: 'Цена',
      key: 'COST_PER_PIECE',
      dataIndex: 'COST_PER_PIECE',
      sorter: (a, b) => a.COST_PER_PIECE - b.COST_PER_PIECE,
    },
    {
      title: 'Активен',
      key: 'ACTIVE',
      dataIndex: 'ACTIVE',
      sorter: (a, b) => a.ACTIVE - b.ACTIVE,
      render: (active) => (active ? 'да' : 'нет'),
      onFilter: (value, record) => value === record.ACTIVE,
      filters: [
        {
          text: 'Активен',
          value: true,
        },
        {
          text: 'Неактивен',
          value: false,
        },
      ],
    },
    {
      title: 'Действия',
      key: '_id',
      dataIndex: '_id',
      render: (id) => {
        return (
          <>
            <Button type="link" onClick={() => removeProductFlow(id)}>Удалить</Button>
            <Button type="link" onClick={() => Router.push(`products/product?productId=${id}`)}>Редактировать</Button>
          </>
        );
      },
    },
  ];

  return (
    <AdminContentWrapper title="Товары">
      <Table loading={loading} columns={columns} dataSource={products} />
    </AdminContentWrapper>
  );
};

Products.getLayout = getLayoutAdmin;

export default observer(Products);
