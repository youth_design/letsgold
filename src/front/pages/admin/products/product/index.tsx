import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import { Spin } from 'antd';
import { useRouter } from 'next/router';
import getLayoutAdmin from '../../../../layouts/getLayoutAdmin';
import { useProductsStore } from '../../../../providers/rootProvider';
import ProductForm from '../../../../components/productForm/ProductForm';

const Product = () => {
  const productsStore = useProductsStore();
  const router = useRouter();

  const {
    loading, product, getProductFlow,
  } = productsStore;

  useEffect(() => {
    const id = router.query.productId;
    if (id && typeof id === 'string') {
      getProductFlow(id);
    }
  }, []);

  if (loading) return <Spin size="large" />;
  console.log(product);
  return (
    product && <ProductForm product={product} onSave={() => true} />
  );
};

Product.getLayout = getLayoutAdmin;

export default observer(Product);
