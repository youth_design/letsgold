import React from 'react';
import { useRouter } from 'next/router';
import { getOrderById } from '../../api/methods';
import { Order } from '../../../utils/entities';

interface OrdersProps {
  order: Order | false
}

const Orders = (props: OrdersProps) => {
  const { order } = props;

  const router = useRouter();
  const orderId : string = router.query.orderId as string;

  return (
    <div>
      Заказы
      {' '}
      {orderId}
      <div>{JSON.stringify(order)}</div>
    </div>
  );
};

export async function getServerSideProps({ params }) {
  const order = await getOrderById(params?.orderId || '');
  return {
    props: {
      order,
    },
  };
}

export default Orders;
