import React, { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import Router from 'next/router';
import LoginForm from '../../components/loginForm/LoginForm';
import { useAuthStore } from '../../providers/rootProvider';
import ContentContainer from '../../components/conetentContainer/ContentContainer';

const Admin = () => {
  const authStore = useAuthStore();

  useEffect(() => {
    if (authStore.user) {
      Router.push('/admin');
    }
  }, [authStore.user]);

  return (
    <ContentContainer>
      <LoginForm authStore={authStore} />
    </ContentContainer>
  );
};

export default observer(Admin);
