import { useEffect } from 'react';
import { observer } from 'mobx-react-lite';
import Router from 'next/router';
import { useAuthStore } from '../providers/rootProvider';

const withAuthRoute = (page, redirect = '/login') => {
  const AuthRoute = () => {
    const { user } = useAuthStore();
    useEffect(() => {
      if (!user) {
        Router.push(redirect);
      }
    }, [user]);

    return page;
  };
  return observer(AuthRoute);
};

export default withAuthRoute;
