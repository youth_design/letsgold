import React, { ReactElement } from 'react';
import { Layout, Menu } from 'antd';
import withAuthRoute from '../hocs/withAuthRoute';
import NavLink from '../components/navLink/NavLink';

const {
  Header, Content, Footer, Sider,
} = Layout;

type AdminProps = {
  children: ReactElement,
}

const AdminLayout = ({ children } : AdminProps) => {
  return (
    <Layout>
      <Sider
        breakpoint="lg"
        collapsedWidth="0"
        onBreakpoint={(broken) => {
          console.log(broken);
        }}
        onCollapse={(collapsed, type) => {
          console.log(collapsed, type);
        }}
      >
        <div className="logo" />
        <Menu theme="dark" mode="vertical">
          <Menu.Item key="admin">
            <NavLink href="/admin">
              Главная
            </NavLink>
          </Menu.Item>
          <Menu.Item key="products">
            <NavLink href="/admin/products">
              Товары
            </NavLink>
          </Menu.Item>
          <Menu.Item key="orders">
            <NavLink href="/admin/orders">
              Заказы
            </NavLink>
          </Menu.Item>
          <Menu.Item key="stats">
            <NavLink href="/admin/stats">
              Статистика
            </NavLink>
          </Menu.Item>
        </Menu>
      </Sider>
      <Layout>
        <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
        <Content style={{ margin: '24px 16px 0' }}>
          <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
            {children}
          </div>
        </Content>
        <Footer style={{ textAlign: 'center' }} />
      </Layout>
    </Layout>
  );
};

export default function getLayoutAdmin(page: ReactElement) {
  const ProtectedPage = withAuthRoute(page);
  return (
    <AdminLayout>
      <ProtectedPage />
    </AdminLayout>
  );
}
