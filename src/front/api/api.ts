import { CreateNotification } from '../components/notificationsProvider/NotificationsProvider';
import { refreshTokens } from './methods';
import { getToken, setTokens } from '../helpers/Utils';

const host = `http://localhost:${process.env.BACKEND_PORT || process.env.NEXT_PUBLIC_BACKEND_PORT}/api`;

let createNotification: CreateNotification | null = null;

export const passCreateNotification = (
  _createNotification: CreateNotification,
): void => {
  createNotification = _createNotification;
};

const spawnErrorNotification = (message = '') => {
  if (createNotification) {
    createNotification({
      timeout: 3000,
      content: message || 'Возникла неизвестная техническая ошибка',
      variant: 'error',
    });
  }
};

const parseResponse = async (response: Response) => {
  const json = await response.json();
  if (json?.error?.message) {
    spawnErrorNotification(json?.error?.message);
  }
  return json;
};

export const apiRequest = async (
  handler: string,
  params?: RequestInit,
  headersParam: HeadersInit = {},
) => {
  const ACCESS_TOKEN = getToken('ACCESS_TOKEN');
  const authHeader = ACCESS_TOKEN ? { Authorization: `Bearer ${ACCESS_TOKEN}` } : {};
  const headers: HeadersInit = { ...headersParam, ...authHeader };
  if (params?.method?.toLocaleLowerCase() === 'post') {
    headers['Content-Type'] = 'application/json';
  }
  const response = await fetch(`${host}${handler}`, { headers, ...params });
  const responseParsed = await parseResponse(response);
  if (responseParsed?.error?.code === 401) {
    const REFRESH_TOKENS = await refreshTokens();
    if (!REFRESH_TOKENS) {
      return responseParsed;
    }
    const { REFRESH_TOKEN, ACCESS_TOKEN: ACCESS_TOKEN_NEW } = REFRESH_TOKENS;
    setTokens(ACCESS_TOKEN_NEW, REFRESH_TOKEN);
    const newHeaders = { ...headers, Authorization: `Bearer ${ACCESS_TOKEN_NEW}` };
    const newResponse = await fetch(`${host}${handler}`, { headers: newHeaders, ...params });
    return parseResponse(newResponse);
  }
  return responseParsed?.body || false;
};
