import {
  AuthResponse,
  LoginResponse, Order, Product, SuccessBody, TokensResponse, User,
} from '../../utils/entities';
import { apiRequest } from './api';
import { getToken } from '../helpers/Utils';

export const getAllProducts = async (): Promise<Product[]> => {
  try {
    return await apiRequest('/products');
  } catch (e) {
    console.error('Error', e);
    return [];
  }
};

export const getProductById = async (id: string) : Promise<Product | false> => {
  try {
    const product: Product | false = await apiRequest(`/products/${id}`);
    if (product) {
      return product;
    }
  } catch (e) {
    console.error('Error');
  }

  return false;
};

export const removeProduct = async (id: string) : Promise<SuccessBody | false> => {
  if (!id) throw Error('id required');
  const body = JSON.stringify({ id });
  try {
    return await apiRequest('/products/delete', { method: 'POST', body });
  } catch (e) {
    return false;
  }
};

export const createOrder = async (order: Order) : Promise<string | false> => {
  try {
    const response = await apiRequest('/orders/', { method: 'POST', body: JSON.stringify(order) });
    if (response?.result) {
      return response?.result;
    }
  } catch (e) {
    console.log('Error', e);
  }

  return false;
};

export const getOrderById = async (orderId: string): Promise<Order | false> => {
  try {
    const response = await apiRequest(`/orders/${orderId}`, { method: 'GET' });
    if (response) {
      return response;
    }
  } catch (e) {
    console.log(e);
  }
  return false;
};

export const login = async (user: User) : Promise<LoginResponse | false> => {
  try {
    return await apiRequest('/auth/login', { method: 'POST', body: JSON.stringify(user) });
  } catch (e) {
    return false;
  }
};

export const logout = async (REFRESH_TOKEN: string) : Promise<LoginResponse | false> => {
  try {
    return await apiRequest(
      '/auth/logout',
      {
        method: 'POST',
        body: JSON.stringify({ REFRESH_TOKEN }),
      },
    );
  } catch (e) {
    return false;
  }
};

export const authByToken = async () : Promise<AuthResponse | false> => {
  try {
    return await apiRequest('/auth/byToken', { method: 'POST' });
  } catch (e) {
    return false;
  }
};

export const refreshTokens = async () : Promise<TokensResponse | false> => {
  const REFRESH_TOKEN = getToken('REFRESH_TOKEN');
  if (!REFRESH_TOKEN) return false;
  const body = JSON.stringify({ REFRESH_TOKEN });
  try {
    return await apiRequest('/auth/token', { method: 'POST', body });
  } catch (e) {
    return false;
  }
};
