import { Request } from 'express';
import { Jwt } from 'jsonwebtoken';

interface FormFieldParams {
    name: string,
    id: string,
    placeholder: string,
    required?: boolean
}

export interface TextFieldParams extends FormFieldParams {
    type: 'text',
}

export interface EmailFieldParams extends FormFieldParams {
    type: 'email',
}

export interface SelectOption {
    value: string,
    label: string,
}

export interface SelectFieldParams extends FormFieldParams {
    type: 'select',
    options: SelectOption[],
}

export interface Product {
    NAME: string,
    DESCRIPTION?: string,
    COST_PER_PIECE: number,
    IMAGE_PATH?: string,
    INCREMENT_MULTIPLIER: number,
    _id?: string,
    ORDER_FORM_FIELDS: (TextFieldParams | EmailFieldParams | SelectFieldParams)[],
    ACTIVE: boolean,
}

export interface FormValues {
    [x: string]: string,
}

export interface Order {
    PRODUCT_ID: string,
    COUNT: number,
    FORM_VALUES: FormValues,
}

export interface UserData {
    NAME?: string,
}

export interface User extends UserData {
    PASSWORD: string,
    IS_ADMIN?: boolean,
    REFRESH_TOKENS?: string[],
}

export interface RequestAuthenticateToken extends Request {
    user?: UserData
}

export interface UserDataJwt extends Jwt, UserData {}

export interface LoginResponse {
    ACCESS_TOKEN: string,
    REFRESH_TOKEN: string,
    IS_ADMIN: boolean,
}

export interface AuthResponse {
    NAME: string,
    IS_ADMIN: boolean,
}

export interface TokensResponse {
    ACCESS_TOKEN: string,
    REFRESH_TOKEN: string,
}

export interface SuccessBody {
    message: 'OK',
}
